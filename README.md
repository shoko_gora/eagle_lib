# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Private Library for Eagle
* V1.1
* List of components:
NEO6/NEO7
BH1750FVI
BME280
INA219
1825010-5 ->TE Connectivity Slide Switches - ASE and ASF Series
0805 -> 0805 Range of Capacitor
CR2032 -> Low Profile Coin Cell Holder
CSTCE8M00G52A-R0 -> Ceramic Resonators 8MHz
DLG-0504-4R7 -> inductor coil
ESP01 -> ESP8266 Wifi module 01
ESP12 -> ESP8266 Wifi module 12
ESP12E ->ESP8266-12E with additional I/O and GPIO04/05 corrected 
JUMPER_2WAY -> in various form, PCB solder jumper and physical resistor
LED 1210 -> Lite-On C930 Series Chip LED
LFXTAL003000 -> CRYSTALS WITH CAN GROUNDED
MCP1642B -> Synchronous Boost Converter
MINI-USB -> USB On-The-Go (OTG)
NCP1117ST33T3G -> LDO (Low Dropout) Linear Voltage Regulator - ON Semiconductor
PNP -> transitor BC807
R0805 -> Resistor R0805 package
RTRIM -> 200mW 4mm Open Frame 3364W Series
STM32F103C8T6 -> STMicroelectronics STM32F103C8T6, 32bit ARM Cortex M3 Microcontroller, 72MHz, 64 kB Flash, 48-pin LQFP
SWITCH -> Apem DTSM-6 Series Tact Switches
T821M114A1S100CEU-B -> socket SMT
TL431 -> VOLTAGE REGULATOR

### How do I get set up? ###

* Download
* Place in EAGLE lib folder
* Enjoy


### Who do I talk to? ###

* Marcin Gajewski